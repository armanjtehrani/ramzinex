from rest_framework import generics

from .models import Order, Wallet
from .serializers import OrderRetrieveSerializer, WalletSerializer


# Create your views here.
class WalletListCreateView(generics.ListCreateAPIView):
    serializer_class = WalletSerializer

    def get_serializer_context(self):
        context = super().get_serializer_context()
        context["user_id"] = self.kwargs["uid"]
        return context

    def get_queryset(self):
        return Wallet.objects.filter(owner_id=self.kwargs["uid"]).prefetch_related(
            "currencies"
        )


class WalletRetrieveUpdateDeleteView(generics.RetrieveUpdateAPIView):
    lookup_url_kwarg = "wid"
    serializer_class = WalletSerializer

    def get_serializer(self, *args, **kwargs):
        return super().get_serializer(*args, **kwargs)

    def get_queryset(self):
        return Wallet.objects.filter(id=self.kwargs["wid"]).prefetch_related(
            "currencies"
        )


class OrderRetrieveView(generics.RetrieveDestroyAPIView):
    lookup_url_kwarg = "oid"
    serializer_class = OrderRetrieveSerializer

    def get_queryset(self):
        return Order.objects.filter(id=self.kwargs["oid"]).prefetch_related(
            "confirmations"
        )

    def inform_bazaar(self, order_id: int) -> None:
        pass
