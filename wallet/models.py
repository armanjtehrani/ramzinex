from django.conf import settings
from django.db import models
from django.utils.translation import gettext_lazy as _

from . import consts


class Currency(models.TextChoices):
    BTC = "BTC", _("Bitcoin")
    RIALS = "RIALS", _("Rials")
    ETH = "ETH", _("Etherium")
    USDT = "USDT", _("USDollar")


class Wallet(models.Model):
    owner = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.DO_NOTHING)
    name = models.CharField(max_length=15)

    class Meta:
        unique_together = [["owner", "name"]]


class CurrencyWallet(models.Model):
    wallet = models.ForeignKey(
        Wallet, on_delete=models.DO_NOTHING, related_name="currencies"
    )

    currency = models.CharField(max_length=10, choices=Currency.choices, db_index=True)

    total_amount = models.DecimalField(
        default=0,
        max_digits=consts.CURRENCY_AMOUNT_MAX_DIGITS,
        decimal_places=consts.CURRENCY_AMOUNT_DECIMAL_PLACE,
    )
    available_amount = models.DecimalField(
        default=0,
        max_digits=consts.CURRENCY_AMOUNT_MAX_DIGITS,
        decimal_places=consts.CURRENCY_AMOUNT_DECIMAL_PLACE,
    )

    class Meta:
        unique_together = [["wallet", "currency"]]


class Order(models.Model):
    wallet = models.ForeignKey(
        Wallet, on_delete=models.DO_NOTHING, related_name="orders"
    )
    base_currency = models.CharField(
        max_length=10, choices=Currency.choices, db_index=True
    )
    base_amount = models.DecimalField(
        max_digits=consts.CURRENCY_AMOUNT_MAX_DIGITS,
        decimal_places=consts.CURRENCY_AMOUNT_DECIMAL_PLACE,
    )

    quote_currency = models.CharField(
        max_length=10, choices=Currency.choices, db_index=True
    )
    quote_amount = models.DecimalField(
        max_digits=consts.CURRENCY_AMOUNT_MAX_DIGITS,
        decimal_places=consts.CURRENCY_AMOUNT_DECIMAL_PLACE,
    )
    is_completed = models.BooleanField(default=False, db_index=True)
    is_canceled = models.BooleanField(default=False, db_index=True)

    @property
    def is_done(self) -> bool:
        return self.is_completed | self.is_canceled


class OrderConfirmation(models.Model):
    order = models.ForeignKey(
        Order, on_delete=models.DO_NOTHING, related_name="confirmations"
    )
    base_currency = models.CharField(
        max_length=10, choices=Currency.choices, db_index=True
    )
    base_amount = models.DecimalField(
        max_digits=consts.CURRENCY_AMOUNT_MAX_DIGITS,
        decimal_places=consts.CURRENCY_AMOUNT_DECIMAL_PLACE,
    )

    quote_currency = models.CharField(
        max_length=10, choices=Currency.choices, db_index=True
    )
    quote_amount = models.DecimalField(
        max_digits=consts.CURRENCY_AMOUNT_MAX_DIGITS,
        decimal_places=consts.CURRENCY_AMOUNT_DECIMAL_PLACE,
    )
