# Generated by Django 4.2.7 on 2023-11-05 17:27

import django.db.models.deletion
from django.conf import settings
from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ("wallet", "0004_alter_currencywallet_wallet"),
    ]

    operations = [
        migrations.AddField(
            model_name="order",
            name="is_done",
            field=models.BooleanField(db_index=True, default=False),
        ),
        migrations.AlterField(
            model_name="currencywallet",
            name="wallet",
            field=models.ForeignKey(
                on_delete=django.db.models.deletion.DO_NOTHING,
                related_name="currencies",
                to="wallet.wallet",
            ),
        ),
        migrations.AlterField(
            model_name="order",
            name="wallet",
            field=models.ForeignKey(
                on_delete=django.db.models.deletion.DO_NOTHING,
                related_name="orders",
                to="wallet.wallet",
            ),
        ),
        migrations.AlterField(
            model_name="wallet",
            name="owner",
            field=models.ForeignKey(
                on_delete=django.db.models.deletion.DO_NOTHING,
                to=settings.AUTH_USER_MODEL,
            ),
        ),
        migrations.CreateModel(
            name="FinishedOrder",
            fields=[
                (
                    "id",
                    models.BigAutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                (
                    "base_currency",
                    models.CharField(
                        choices=[
                            ("BTC", "Bitcoin"),
                            ("RIALS", "Rials"),
                            ("ETH", "Etherium"),
                            ("USDT", "USDollar"),
                        ],
                        db_index=True,
                        max_length=10,
                    ),
                ),
                ("base_amount", models.DecimalField(decimal_places=6, max_digits=15)),
                (
                    "quote_currency",
                    models.CharField(
                        choices=[
                            ("BTC", "Bitcoin"),
                            ("RIALS", "Rials"),
                            ("ETH", "Etherium"),
                            ("USDT", "USDollar"),
                        ],
                        db_index=True,
                        max_length=10,
                    ),
                ),
                ("quote_amount", models.DecimalField(decimal_places=6, max_digits=15)),
                (
                    "order",
                    models.ForeignKey(
                        on_delete=django.db.models.deletion.DO_NOTHING,
                        related_name="finished_orders",
                        to="wallet.order",
                    ),
                ),
            ],
        ),
    ]
