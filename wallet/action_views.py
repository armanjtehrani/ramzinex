from dataclasses import dataclass
from decimal import Decimal

import pgtransaction
from django.db.models import F, Sum
from rest_framework import generics
from rest_framework.exceptions import ValidationError
from rest_framework.response import Response
from rest_framework_dataclasses.serializers import DataclassSerializer

from . import consts
from .models import Currency, CurrencyWallet, Order, OrderConfirmation
from .serializers import OrderRetrieveSerializer


@dataclass
class WalletTransaction:
    currency: Currency
    amount: Decimal


class WalletTransactionSerializer(DataclassSerializer):
    class Meta:
        dataclass = WalletTransaction
        extra_kwargs = {
            "amount": {
                "decimal_places": consts.CURRENCY_AMOUNT_DECIMAL_PLACE,
                "max_digits": consts.CURRENCY_AMOUNT_MAX_DIGITS,
            }
        }


class DepositionView(generics.CreateAPIView):
    def create(self, request, *args, **kwargs):
        request_serializer = WalletTransactionSerializer(data=request.data)
        request_serializer.is_valid(raise_exception=True)
        deposit: WalletTransaction = request_serializer.validated_data

        self.update_wallet(int(kwargs["wid"]), deposit)
        return Response()

    @staticmethod
    def update_wallet(wallet_id: int, deposit: WalletTransaction) -> None:
        _, created = CurrencyWallet.objects.get_or_create(
            wallet_id=wallet_id,
            currency=deposit.currency,
            defaults={
                "total_amount": deposit.amount,
                "available_amount": deposit.amount,
            },
        )
        if not created:
            CurrencyWallet.objects.filter(
                wallet_id=wallet_id,
                currency=deposit.currency,
            ).update(
                total_amount=F("total_amount") + deposit.amount,
                available_amount=F("available_amount") + deposit.amount,
            )


class WithdrawalView(generics.CreateAPIView):
    def create(self, request, *args, **kwargs):
        request_serializer = WalletTransactionSerializer(data=request.data)
        request_serializer.is_valid(raise_exception=True)
        withdrawal: WalletTransaction = request_serializer.validated_data

        self.update_wallet(int(self.kwargs["wid"]), withdrawal)
        return Response()

    @staticmethod
    def update_wallet(wallet_id: int, withdrawal: WalletTransaction) -> None:
        updates = CurrencyWallet.objects.filter(
            wallet_id=wallet_id,
            currency=withdrawal.currency,
            available_amount__gte=withdrawal.amount,
        ).update(
            total_amount=F("total_amount") - withdrawal.amount,
            available_amount=F("available_amount") - withdrawal.amount,
        )
        if updates == 0:
            raise ValidationError(
                f"Currency {withdrawal.currency} available amount is less than {withdrawal.amount}"
            )


@dataclass
class OrderTransaction:
    base_currency: WalletTransaction
    quote_currency: WalletTransaction


class OrderTransactionSerializer(DataclassSerializer):
    class Meta:
        dataclass = OrderTransaction

    def validate(self, attrs: OrderTransaction):
        base_curr = attrs.base_currency.currency
        quote_curr = attrs.quote_currency.currency
        if base_curr == quote_curr:
            raise ValidationError(f"base and quote currencies must be different")
        return attrs


class OrderListCreateView(generics.ListCreateAPIView):
    serializer_class = OrderRetrieveSerializer

    def get_queryset(self):
        return Order.objects.filter(wallet_id=self.kwargs["wid"]).prefetch_related(
            "confirmations"
        )

    def create(self, request, *args, **kwargs):
        request_serializer = OrderTransactionSerializer(data=request.data)
        request_serializer.is_valid(raise_exception=True)
        order: OrderTransaction = request_serializer.validated_data
        self.set_order(int(self.kwargs["wid"]), order)
        return Response()

    @pgtransaction.atomic(isolation_level=pgtransaction.SERIALIZABLE)
    def set_order(self, wallet_id: int, order: OrderTransaction) -> None:
        # decrease available amount
        updates = CurrencyWallet.objects.filter(
            wallet_id=wallet_id,
            currency=order.base_currency.currency,
            available_amount__gte=order.base_currency.amount,
        ).update(available_amount=F("available_amount") - order.base_currency.amount)
        if updates == 0:
            raise ValidationError(
                f"Currency {order.base_currency.currency} "
                f"available amount is less than {order.base_currency.amount}"
            )
        # create order if successful
        order_object = Order.objects.create(
            wallet_id=wallet_id,
            base_currency=order.base_currency.currency,
            base_amount=order.base_currency.amount,
            quote_currency=order.quote_currency.currency,
            quote_amount=order.quote_currency.amount,
        )

        self.inform_bazaar(order_object.id, order)

    def inform_bazaar(self, order_id: int, order: OrderTransaction) -> None:
        pass


class OrderCancellationView(generics.UpdateAPIView):
    def update(self, request, *args, **kwargs):
        order_id = kwargs["oid"]

        self.cancel_order(order_id)

        return Response()

    @pgtransaction.atomic(isolation_level=pgtransaction.SERIALIZABLE)
    def cancel_order(self, order_id: int) -> None:
        order = Order.objects.filter(id=order_id).first()
        if order is None:
            raise ValidationError(f"Order {order_id} doesn't exist")
        if order.is_done:
            raise ValidationError(
                f"Order {order_id} is already done and can't be cancelled"
            )
        order.is_canceled = True
        order.save()

        base_currency = order.base_currency
        wallet_id = order.wallet_id
        confirmed = OrderConfirmation.objects.filter(order_id=order_id).aggregate(
            Sum("base_amount")
        )["base_amount__sum"]
        if confirmed is None:
            confirmed = 0
        leftover_amount = order.base_amount - confirmed
        CurrencyWallet.objects.filter(
            wallet_id=wallet_id, currency=base_currency
        ).update(available_amount=F("available_amount") + leftover_amount)

        self.inform_bazaar(order_id)

    def inform_bazaar(self, order_id: int) -> None:
        pass


class OrderConfirmationView(generics.CreateAPIView):
    def create(self, request, *args, **kwargs):
        request_serializer = OrderTransactionSerializer(data=request.data)
        request_serializer.is_valid(raise_exception=True)
        order: OrderTransaction = request_serializer.validated_data
        self.confirm_order(int(self.kwargs["oid"]), order)
        return Response()

    @pgtransaction.atomic(isolation_level=pgtransaction.SERIALIZABLE)
    def confirm_order(self, order_id: int, order_confirmation: OrderTransaction):
        order_dict = Order.objects.filter(id=order_id).values("wallet_id").first()
        if order_dict is None:
            raise ValidationError(f"Order {order_id} doesn't exist.")
        wallet_id = order_dict["wallet_id"]
        # update wallets
        # available amount is already decreased!
        CurrencyWallet.objects.filter(
            wallet_id=wallet_id,
            currency=order_confirmation.base_currency.currency,
        ).update(
            total_amount=F("total_amount") - order_confirmation.base_currency.amount
        )
        #
        _, created = CurrencyWallet.objects.get_or_create(
            wallet_id=wallet_id,
            currency=order_confirmation.quote_currency.currency,
            defaults={
                "total_amount": order_confirmation.quote_currency.amount,
                "available_amount": order_confirmation.quote_currency.amount,
            },
        )
        if not created:
            CurrencyWallet.objects.filter(
                wallet_id=wallet_id,
                currency=order_confirmation.quote_currency.currency,
            ).update(
                total_amount=F("total_amount")
                + order_confirmation.quote_currency.amount,
                available_amount=F("available_amount")
                + order_confirmation.quote_currency.amount,
            )
        #
        OrderConfirmation.objects.create(
            order_id=order_id,
            base_currency=order_confirmation.base_currency.currency,
            base_amount=order_confirmation.base_currency.amount,
            quote_currency=order_confirmation.quote_currency.currency,
            quote_amount=order_confirmation.quote_currency.amount,
        )

        total_base_amount = OrderConfirmation.objects.filter(
            order_id=order_id
        ).aggregate(Sum("base_amount"))["base_amount__sum"]
        Order.objects.filter(id=order_id, base_amount=total_base_amount).update(
            is_completed=True
        )
