from decimal import Decimal

from django.contrib.auth.models import User
from django.test import Client, TestCase

from . import consts
from .models import Currency, CurrencyWallet, Order, OrderConfirmation, Wallet


def get_wallet_currencies(wallet_id) -> list[CurrencyWallet]:
    currencies_model = []
    for currency in Currency:
        currency_model = None
        try:
            currency_model = CurrencyWallet.objects.filter(
                wallet_id=wallet_id, currency=currency
            ).get()
        except:
            currency_model = CurrencyWallet(wallet_id=wallet_id, currency=currency)
        finally:
            currencies_model.append(currency_model)
    return currencies_model


# Create your tests here.
class WalletTestCase(TestCase):
    def setUp(self):
        self.users = [
            User.objects.create(username="test0"),
            User.objects.create(username="test1"),
        ]
        self.wallets = [
            Wallet.objects.create(owner=self.users[0], name="u0w0"),
            Wallet.objects.create(owner=self.users[0], name="u0w1"),
            Wallet.objects.create(owner=self.users[0], name="u0w2"),
        ]

        self.client = Client(SERVER_NAME="localhost")

    def check_currency_wallet(
        self, currency_dict: dict, currency_model: CurrencyWallet
    ) -> None:
        self.assertEqual(currency_model.currency, currency_dict["currency"])
        self.assertEqual(
            currency_model.total_amount, Decimal(currency_dict["total_amount"])
        )
        self.assertEqual(
            currency_model.available_amount, Decimal(currency_dict["available_amount"])
        )

    def check_wallet(self, wallet_dict: dict, wallet_model: Wallet) -> None:
        self.assertEqual(wallet_dict["name"], wallet_model.name)
        self.assertEqual(wallet_dict["owner_id"], wallet_model.owner_id)
        currencies_dict = wallet_dict["currencies"]

        currencies_model = get_wallet_currencies(wallet_model.id)

        self.assertEqual(len(currencies_dict.keys()), len(Currency))
        for currency_model in currencies_model:
            currency_dict = currencies_dict.get(currency_model.currency)
            self.assertIsNotNone(currency_dict)
            self.check_currency_wallet(currency_dict, currency_model)

    def test_wallet_list(self):
        user_id = self.users[0].id
        response = self.client.get(f"/api/v1/users/{user_id}/wallets/")
        self.assertEqual(200, response.status_code)

        response_list = response.json()
        response_list.sort(key=lambda response_dict: response_dict["id"])
        self.assertEqual(len(self.wallets), len(response_list))

        for wallet_dict, wallet_model in zip(response_list, self.wallets):
            self.check_wallet(wallet_dict, wallet_model)

    def test_wallet_retrieve(self):
        for wallet_model in self.wallets:
            response = self.client.get(f"/api/v1/wallets/{wallet_model.id}/")
            self.assertEqual(200, response.status_code)

            response_dict = response.json()

            self.check_wallet(response_dict, wallet_model)

    def test_wallet_create(self):
        existing_wallet = self.wallets[0]
        response = self.client.post(
            f"/api/v1/users/{self.users[0].id}/wallets/",
            data={"name": existing_wallet.name},
            content_type="application/json",
        )
        self.assertEqual(400, response.status_code)
        self.assertEqual(len(self.wallets), len(Wallet.objects.all()))

        new_name = "u0w3"
        response = self.client.post(
            f"/api/v1/users/{self.users[0].id}/wallets/",
            data={"name": new_name},
            content_type="application/json",
        )
        self.assertEqual(201, response.status_code)

        new_wallet = Wallet.objects.filter(
            owner_id=self.users[0].id, name=new_name
        ).first()
        self.assertIsNotNone(new_wallet)

        self.assertFalse(CurrencyWallet.objects.filter(wallet=new_wallet).exists())

    def test_wallet_update(self):
        for index, wallet_model in enumerate(self.wallets):
            expected_name = f"{index}-{wallet_model.name}"
            response = self.client.put(
                f"/api/v1/wallets/{wallet_model.id}/",
                data={"name": expected_name, "owner_id": self.users[1].id},
                content_type="application/json",
            )
            self.assertEqual(200, response.status_code)
            new_wallet = Wallet.objects.get(id=wallet_model.id)
            self.assertEqual(expected_name, new_wallet.name)
            self.assertEqual(self.users[1].id, new_wallet.owner_id)

    def test_wallet_update_duplicate_name(self):
        wallet_0 = self.wallets[0]
        wallet_1 = self.wallets[1]
        response = self.client.put(
            f"/api/v1/wallets/{wallet_1.id}/",
            data={"name": wallet_0.name},
            content_type="application/json",
        )
        self.assertEqual(400, response.status_code)

    def test_wallet_delete(self):
        wallet = self.wallets[0]
        response = self.client.delete(f"/api/v1/wallets/{wallet.id}/")
        self.assertEqual(response.status_code, 405)

    def test_wallet_deposition_overflow(self):
        wallet = self.wallets[0]
        btc_currency_amount = Decimal(
            "1." + "5" * (consts.CURRENCY_AMOUNT_DECIMAL_PLACE + 1)
        )
        response = self.client.post(
            f"/api/v1/wallets/{wallet.id}/deposition/",
            data={"currency": Currency.BTC, "amount": btc_currency_amount},
            content_type="application/json",
        )
        self.assertEqual(400, response.status_code)
        self.assertIsNone(
            CurrencyWallet.objects.filter(wallet=wallet, currency=Currency.BTC).first()
        )

        rials_currency_amount = Decimal(
            "1"
            * (
                consts.CURRENCY_AMOUNT_MAX_DIGITS
                - consts.CURRENCY_AMOUNT_DECIMAL_PLACE
                + 1
            )
        )
        response = self.client.post(
            f"/api/v1/wallets/{wallet.id}/deposition/",
            data={"currency": Currency.RIALS, "amount": rials_currency_amount},
            content_type="application/json",
        )
        self.assertEqual(400, response.status_code)
        self.assertIsNone(
            CurrencyWallet.objects.filter(
                wallet=wallet, currency=Currency.RIALS
            ).first()
        )

    def test_wallet_deposition(self):
        wallet = self.wallets[0]
        btc_currency_amount = Decimal("0." + "5" * consts.CURRENCY_AMOUNT_DECIMAL_PLACE)
        response = self.client.post(
            f"/api/v1/wallets/{wallet.id}/deposition/",
            data={"currency": Currency.BTC, "amount": btc_currency_amount},
            content_type="application/json",
        )
        self.assertEqual(200, response.status_code)

        currencies = CurrencyWallet.objects.filter(
            wallet=wallet, currency=Currency.BTC
        ).all()
        self.assertEqual(1, len(currencies))

        currency = currencies[0]
        self.assertIsNotNone(currency)
        self.assertEqual(btc_currency_amount, currency.total_amount)
        self.assertEqual(btc_currency_amount, currency.available_amount)

        response = self.client.post(
            f"/api/v1/wallets/{wallet.id}/deposition/",
            data={"currency": Currency.BTC, "amount": btc_currency_amount},
            content_type="application/json",
        )
        self.assertEqual(200, response.status_code)

        currencies = CurrencyWallet.objects.filter(
            wallet=wallet, currency=Currency.BTC
        ).all()
        self.assertEqual(1, len(currencies))

        currency = currencies[0]
        self.assertIsNotNone(currency)
        self.assertEqual(2 * btc_currency_amount, currency.total_amount)
        self.assertEqual(2 * btc_currency_amount, currency.available_amount)

        rials_currency_amount = Decimal(
            "1"
            * (consts.CURRENCY_AMOUNT_MAX_DIGITS - consts.CURRENCY_AMOUNT_DECIMAL_PLACE)
        )
        response = self.client.post(
            f"/api/v1/wallets/{wallet.id}/deposition/",
            data={"currency": Currency.RIALS, "amount": rials_currency_amount},
            content_type="application/json",
        )
        self.assertEqual(200, response.status_code)

        currency = CurrencyWallet.objects.filter(
            wallet=wallet, currency=Currency.RIALS
        ).first()
        self.assertIsNotNone(currency)
        self.assertEqual(rials_currency_amount, currency.total_amount)
        self.assertEqual(rials_currency_amount, currency.available_amount)

        response = self.client.post(
            f"/api/v1/wallets/{wallet.id}/deposition/",
            data={"currency": Currency.RIALS, "amount": rials_currency_amount},
            content_type="application/json",
        )
        self.assertEqual(200, response.status_code)

        currency = CurrencyWallet.objects.filter(
            wallet=wallet, currency=Currency.RIALS
        ).first()
        self.assertIsNotNone(currency)
        self.assertEqual(2 * rials_currency_amount, currency.total_amount)
        self.assertEqual(2 * rials_currency_amount, currency.available_amount)

    def test_wallet_withdrawal_no_currency(self):
        wallet = self.wallets[0]
        btc_currency_amount = Decimal(1)
        response = self.client.post(
            f"/api/v1/wallets/{wallet.id}/withdrawal/",
            data={"currency": Currency.BTC, "amount": btc_currency_amount},
            content_type="application/json",
        )
        self.assertEqual(400, response.status_code)

        self.assertIsNone(
            CurrencyWallet.objects.filter(wallet=wallet, currency=Currency.BTC).first()
        )

    def test_wallet_withdrawal(self):
        wallet = self.wallets[0]
        btc_currency_amount = Decimal(1)

        CurrencyWallet.objects.create(
            wallet_id=wallet.id,
            currency=Currency.BTC,
            total_amount=2 * btc_currency_amount,
            available_amount=btc_currency_amount,
        )

        response = self.client.post(
            f"/api/v1/wallets/{wallet.id}/withdrawal/",
            data={"currency": Currency.BTC, "amount": btc_currency_amount},
            content_type="application/json",
        )
        self.assertEqual(200, response.status_code)

        curr = CurrencyWallet.objects.filter(
            wallet=wallet, currency=Currency.BTC
        ).first()
        self.assertEqual(btc_currency_amount, curr.total_amount)
        self.assertEqual(0, curr.available_amount)

        response = self.client.post(
            f"/api/v1/wallets/{wallet.id}/withdrawal/",
            data={"currency": Currency.BTC, "amount": btc_currency_amount},
            content_type="application/json",
        )
        self.assertEqual(400, response.status_code)

        curr = CurrencyWallet.objects.filter(
            wallet=wallet, currency=Currency.BTC
        ).first()
        self.assertEqual(btc_currency_amount, curr.total_amount)
        self.assertEqual(0, curr.available_amount)


class OrderCreateTestCase(TestCase):
    def setUp(self):
        self.user = User.objects.create(username="test0")

        self.wallet = Wallet.objects.create(owner=self.user, name="u0w0")

        cw0 = CurrencyWallet.objects.create(
            wallet=self.wallet,
            currency=Currency.BTC,
            total_amount=2.5,
            available_amount=0.5,
        )
        cw1 = CurrencyWallet.objects.create(
            wallet=self.wallet,
            currency=Currency.USDT,
            total_amount=40.5,
            available_amount=25,
        )
        self.currency_wallets = {
            cw0.id: cw0,
            cw1.id: cw1,
        }

        self.client = Client(SERVER_NAME="localhost")

    def check_currency_wallets(
        self, wallet_id: int, all_expected: dict[int, CurrencyWallet]
    ):
        currency_wallets = CurrencyWallet.objects.filter(wallet_id=wallet_id)
        self.assertEqual(len(all_expected), len(currency_wallets))
        for actual in currency_wallets:
            expected = all_expected.get(actual.id)
            self.assertIsNotNone(expected)
            self.assertEqual(expected.currency, actual.currency)
            self.assertEqual(expected.total_amount, actual.total_amount)
            self.assertEqual(expected.available_amount, actual.available_amount)

    def test_order_no_base_currency(self):
        wallet = self.wallet
        order = {
            "base_currency": {"currency": Currency.RIALS, "amount": Decimal(5000)},
            "quote_currency": {"currency": Currency.USDT, "amount": Decimal(50.5)},
        }

        response = self.client.post(
            f"/api/v1/wallets/{wallet.id}/orders/",
            data=order,
            content_type="application/json",
        )
        self.assertEqual(400, response.status_code)
        self.assertEqual(0, Order.objects.count())
        self.check_currency_wallets(
            wallet_id=wallet.id, all_expected=self.currency_wallets
        )

    def test_order_little_available_amount(self):
        wallet = self.wallet
        order = {
            "base_currency": {"currency": Currency.BTC, "amount": Decimal(1)},
            "quote_currency": {"currency": Currency.USDT, "amount": Decimal(10000.5)},
        }

        response = self.client.post(
            f"/api/v1/wallets/{wallet.id}/orders/",
            data=order,
            content_type="application/json",
        )
        self.assertEqual(400, response.status_code)
        self.assertEqual(0, Order.objects.count())
        self.check_currency_wallets(
            wallet_id=wallet.id, all_expected=self.currency_wallets
        )

    def test_same_base_currency(self):
        wallet = self.wallet
        order = {
            "base_currency": {"currency": Currency.USDT, "amount": Decimal(10)},
            "quote_currency": {"currency": Currency.USDT, "amount": Decimal(10000)},
        }

        response = self.client.post(
            f"/api/v1/wallets/{wallet.id}/orders/",
            data=order,
            content_type="application/json",
        )
        self.assertEqual(400, response.status_code)
        self.assertEqual(0, Order.objects.count())
        self.check_currency_wallets(
            wallet_id=wallet.id, all_expected=self.currency_wallets
        )

    def test_order(self):
        self.assertEqual(0, Order.objects.count())
        wallet = self.wallet
        order = {
            "base_currency": {"currency": Currency.USDT, "amount": Decimal(10)},
            "quote_currency": {"currency": Currency.RIALS, "amount": Decimal(5000000)},
        }

        all_expected = {}
        for cw in self.currency_wallets.values():
            if cw.currency == order["base_currency"]["currency"]:
                cw.available_amount -= order["base_currency"]["amount"]
            all_expected[cw.id] = cw

        response = self.client.post(
            f"/api/v1/wallets/{wallet.id}/orders/",
            data=order,
            content_type="application/json",
        )
        self.assertEqual(200, response.status_code)
        self.assertEqual(1, Order.objects.count())
        o = Order.objects.last()
        self.assertEqual(self.wallet.id, o.wallet_id)
        self.assertEqual(order["base_currency"]["currency"], o.base_currency)
        self.assertEqual(order["base_currency"]["amount"], o.base_amount)
        self.assertEqual(order["quote_currency"]["currency"], o.quote_currency)
        self.assertEqual(order["quote_currency"]["amount"], o.quote_amount)
        self.assertEqual(False, o.is_completed)
        self.assertEqual(False, o.is_canceled)
        self.check_currency_wallets(wallet.id, all_expected)

        all_expected = {}
        for cw in self.currency_wallets.values():
            if cw.currency == order["base_currency"]["currency"]:
                cw.available_amount -= order["base_currency"]["amount"]
            all_expected[cw.id] = cw

        response = self.client.post(
            f"/api/v1/wallets/{wallet.id}/orders/",
            data=order,
            content_type="application/json",
        )
        self.assertEqual(200, response.status_code)
        self.assertEqual(2, Order.objects.count())
        o = Order.objects.last()
        self.assertEqual(self.wallet.id, o.wallet_id)
        self.assertEqual(order["base_currency"]["currency"], o.base_currency)
        self.assertEqual(order["base_currency"]["amount"], o.base_amount)
        self.assertEqual(order["quote_currency"]["currency"], o.quote_currency)
        self.assertEqual(order["quote_currency"]["amount"], o.quote_amount)
        self.assertEqual(False, o.is_completed)
        self.assertEqual(False, o.is_canceled)
        self.check_currency_wallets(wallet.id, all_expected)

        response = self.client.post(
            f"/api/v1/wallets/{wallet.id}/orders/",
            data=order,
            content_type="application/json",
        )
        self.assertEqual(400, response.status_code)
        self.check_currency_wallets(wallet.id, all_expected)


class OrderTestCase(TestCase):
    def setUp(self):
        self.user = User.objects.create(username="test0")

        self.wallet = Wallet.objects.create(owner=self.user, name="u0w0")

        cw0 = CurrencyWallet.objects.create(
            wallet=self.wallet,
            currency=Currency.BTC,
            total_amount=2.5,
            available_amount=0.5,
        )
        cw1 = CurrencyWallet.objects.create(
            wallet=self.wallet,
            currency=Currency.USDT,
            total_amount=40.5,
            available_amount=25,
        )
        self.currency_wallets = {
            cw0.id: cw0,
            cw1.id: cw1,
        }

        o0 = Order.objects.create(
            wallet=self.wallet,
            base_currency=Currency.BTC,
            base_amount=1,
            quote_currency=Currency.RIALS,
            quote_amount=1000,
        )
        o1 = Order.objects.create(
            wallet=self.wallet,
            base_currency=Currency.BTC,
            base_amount=1,
            quote_currency=Currency.RIALS,
            quote_amount=1500,
        )
        o2 = Order.objects.create(
            wallet=self.wallet,
            base_currency=Currency.USDT,
            base_amount=15.5,
            quote_currency=Currency.ETH,
            quote_amount=15.5,
        )
        self.orders = {
            o0.id: o0,
            o1.id: o1,
            o2.id: o2,
        }
        self.order = o0

        self.client = Client(SERVER_NAME="localhost")

    def check_currency_wallets(
        self, wallet_id: int, all_expected: dict[int, CurrencyWallet]
    ):
        currency_wallets = CurrencyWallet.objects.filter(wallet_id=wallet_id)
        self.assertEqual(len(all_expected), len(currency_wallets))
        for actual in currency_wallets:
            expected = all_expected.get(actual.id)
            self.assertIsNotNone(expected)
            self.assertEqual(expected.currency, actual.currency)
            self.assertEqual(expected.total_amount, actual.total_amount)
            self.assertEqual(expected.available_amount, actual.available_amount)

    def test_order_list(self):
        response = self.client.get(f"/api/v1/wallets/{self.wallet.id}/orders/")
        self.assertEqual(200, response.status_code)
        orders = response.json()
        self.assertEqual(len(self.orders), len(orders))

    def test_confirm_wrong_order(self):
        confirmation = {
            "base_currency": {"currency": Currency.BTC, "amount": Decimal("0.5")},
            "quote_currency": {"currency": Currency.USDT, "amount": Decimal("10000.5")},
        }
        response = self.client.post(
            f"/api/v1/orders/{self.order.id - 1}/confirmations/",
            data=confirmation,
            content_type="application/json",
        )
        self.assertEqual(400, response.status_code)

    def test_confirm(self):
        confirmations = [
            {
                "base_currency": {"currency": Currency.BTC, "amount": Decimal("0.5")},
                "quote_currency": {
                    "currency": Currency.RIALS,
                    "amount": Decimal("500"),
                },
            },
            {
                "base_currency": {"currency": Currency.BTC, "amount": Decimal("0.3")},
                "quote_currency": {
                    "currency": Currency.RIALS,
                    "amount": Decimal("400"),
                },
            },
            {
                "base_currency": {"currency": Currency.BTC, "amount": Decimal("0.2")},
                "quote_currency": {
                    "currency": Currency.RIALS,
                    "amount": Decimal("250"),
                },
            },
        ]

        quote_amount = 0
        for index, confirmation in enumerate(confirmations):
            cw_btc = CurrencyWallet.objects.get(
                wallet=self.wallet, currency=Currency.BTC
            )
            self.assertEqual(False, Order.objects.get(id=self.order.id).is_done)

            response = self.client.post(
                f"/api/v1/orders/{self.order.id}/confirmations/",
                data=confirmation,
                content_type="application/json",
            )
            self.assertEqual(200, response.status_code)

            confirmation_models = OrderConfirmation.objects.filter(order=self.order)
            self.assertEqual(index + 1, len(confirmation_models))
            new_model = confirmation_models.last()
            self.assertEqual(
                confirmation["base_currency"]["currency"], new_model.base_currency
            )
            self.assertEqual(
                confirmation["base_currency"]["amount"], new_model.base_amount
            )
            self.assertEqual(
                confirmation["quote_currency"]["currency"], new_model.quote_currency
            )
            self.assertEqual(
                confirmation["quote_currency"]["amount"], new_model.quote_amount
            )

            actual_cw_btc = CurrencyWallet.objects.get(
                wallet=self.wallet, currency=Currency.BTC
            )
            actual_cw_rials = CurrencyWallet.objects.get(
                wallet=self.wallet, currency=Currency.RIALS
            )

            self.assertEqual(
                cw_btc.total_amount - confirmation["base_currency"]["amount"],
                actual_cw_btc.total_amount,
            )
            self.assertEqual(cw_btc.available_amount, actual_cw_btc.available_amount)

            quote_amount += confirmation["quote_currency"]["amount"]
            self.assertEqual(quote_amount, actual_cw_rials.total_amount)
            self.assertEqual(quote_amount, actual_cw_rials.available_amount)

        self.assertEqual(True, Order.objects.get(id=self.order.id).is_completed)
        self.assertEqual(False, Order.objects.get(id=self.order.id).is_canceled)

    def test_cancellation_wrong_order(self):
        response = self.client.put(f"/api/v1/orders/{self.order.id - 1}/cancellation/")
        self.assertEqual(400, response.status_code)

    def test_cancel_already_cancelled_order(self):
        self.order.is_canceled = True
        self.order.save()
        response = self.client.put(f"/api/v1/orders/{self.order.id}/cancellation/")
        self.assertEqual(400, response.status_code)
        self.check_currency_wallets(self.wallet.id, self.currency_wallets)

    def test_cancel_already_completed_order(self):
        self.order.is_completed = True
        self.order.save()
        response = self.client.put(f"/api/v1/orders/{self.order.id}/cancellation/")
        self.assertEqual(400, response.status_code)
        self.check_currency_wallets(self.wallet.id, self.currency_wallets)

    def test_cancellation_no_confirm(self):
        response = self.client.put(f"/api/v1/orders/{self.order.id}/cancellation/")
        self.assertEqual(200, response.status_code)
        expected_cws = {}
        for cw in self.currency_wallets.values():
            if cw.currency == self.order.base_currency:
                cw.available_amount += self.order.base_amount
            expected_cws[cw.id] = cw
        self.check_currency_wallets(self.wallet.id, expected_cws)

    def test_cancel_partial_confirmed(self):
        confirmations = [
            {
                "base_currency": {"currency": Currency.BTC, "amount": Decimal("0.5")},
                "quote_currency": {
                    "currency": Currency.RIALS,
                    "amount": Decimal("500"),
                },
            },
            {
                "base_currency": {"currency": Currency.BTC, "amount": Decimal("0.3")},
                "quote_currency": {
                    "currency": Currency.RIALS,
                    "amount": Decimal("400"),
                },
            },
        ]
        confirmed_amount = Decimal("0")
        for confirmation in confirmations:
            response = self.client.post(
                f"/api/v1/orders/{self.order.id}/confirmations/",
                data=confirmation,
                content_type="application/json",
            )
            self.assertEqual(200, response.status_code)
            confirmed_amount += confirmation["base_currency"]["amount"]
        bcw = CurrencyWallet.objects.get(wallet=self.wallet, currency=Currency.BTC)
        qcw = CurrencyWallet.objects.get(wallet=self.wallet, currency=Currency.RIALS)

        response = self.client.put(f"/api/v1/orders/{self.order.id}/cancellation/")
        self.assertEqual(200, response.status_code)

        new_bcw = CurrencyWallet.objects.get(wallet=self.wallet, currency=Currency.BTC)
        new_qcw = CurrencyWallet.objects.get(
            wallet=self.wallet, currency=Currency.RIALS
        )
        self.assertEqual(bcw.total_amount, new_bcw.total_amount)
        self.assertEqual(
            bcw.available_amount + self.order.base_amount - confirmed_amount,
            new_bcw.available_amount,
        )
        self.assertEqual(qcw.total_amount, new_qcw.total_amount)
        self.assertEqual(qcw.available_amount, new_qcw.available_amount)
