from django.contrib import admin

from .models import CurrencyWallet, Order, Wallet

# Register your models here.
admin.site.register(Wallet)
admin.site.register(CurrencyWallet)
admin.site.register(Order)
