from django.urls import path

from .action_views import (
    DepositionView,
    OrderCancellationView,
    OrderConfirmationView,
    OrderListCreateView,
    WithdrawalView,
)
from .views import (
    OrderRetrieveView,
    WalletListCreateView,
    WalletRetrieveUpdateDeleteView,
)

urlpatterns = [
    path('users/<uid>/wallets/', WalletListCreateView.as_view(), name="wallet-list"),
    path('wallets/<wid>/', WalletRetrieveUpdateDeleteView.as_view(), name="wallet-item"),
    path('orders/<oid>/', OrderRetrieveView.as_view(), name="order-retrieve"),

    path('wallets/<wid>/deposition/', DepositionView.as_view(), name="wallet-deposition"),
    path('wallets/<wid>/withdrawal/', WithdrawalView.as_view(), name="wallet-withdrawal"),
    path('wallets/<wid>/orders/', OrderListCreateView.as_view(), name="order-create"),
    path('orders/<oid>/cancellation/', OrderCancellationView.as_view(), name="order-cancellation"),
    path('orders/<oid>/confirmations/', OrderConfirmationView.as_view(), name="order-confirmation"),
]
