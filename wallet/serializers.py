from __future__ import annotations

from rest_framework import serializers, validators

from .models import Currency, CurrencyWallet, Order, OrderConfirmation, Wallet


class CurrencyWalletListSerializer(serializers.ListSerializer):
    @property
    def data(self):
        if hasattr(self, "initial_data") and not hasattr(self, "_validated_data"):
            msg = (
                "When a serializer is passed a `data` keyword argument you "
                "must call `.is_valid()` before attempting to access the "
                "serialized `.data` representation.\n"
                "You should either call `.is_valid()` first, "
                "or access `.initial_data` instead."
            )
            raise AssertionError(msg)

        if not hasattr(self, "_data"):
            if self.instance is not None and not getattr(self, "_errors", None):
                self._data = self.to_representation(self.instance)
            elif hasattr(self, "_validated_data") and not getattr(
                self, "_errors", None
            ):
                self._data = self.to_representation(self.validated_data)
            else:
                self._data = self.get_initial()
        return serializers.ReturnDict(self._data, serializer=self)

    def to_representation(self, data):
        data = super().to_representation(data)
        return_data = {}
        for item in data:
            return_data[item["currency"]] = item
        for currency in Currency:
            if currency.value in return_data:
                continue
            empty_curr_wallet_obj = CurrencyWallet(
                currency=currency.value,
            )
            empty_curr_wallet = CurrencyWalletRetrieveSerializer(
                empty_curr_wallet_obj
            ).data
            return_data[currency.value] = empty_curr_wallet
        return return_data


class CurrencyWalletRetrieveSerializer(serializers.ModelSerializer):
    class Meta:
        model = CurrencyWallet
        fields = (
            "currency",
            "total_amount",
            "available_amount",
        )
        read_only_fields = fields
        list_serializer_class = CurrencyWalletListSerializer


class WalletSerializer(serializers.ModelSerializer):
    currencies = CurrencyWalletRetrieveSerializer(many=True, read_only=True)

    class Meta:
        model = Wallet
        fields = (
            "id",
            "owner_id",
            "name",
            "currencies",
        )
        validators = [
            validators.UniqueTogetherValidator(
                queryset=model.objects.all(),
                fields=("owner_id", "name"),
            )
        ]

    def to_internal_value(self, data):
        if self.context.get("user_id"):
            data["owner_id"] = self.context.get("user_id")
        return data


class OrderConfirmationRetrieveSerializer(serializers.ModelSerializer):
    class Meta:
        model = OrderConfirmation
        fields = (
            "id",
            "order_id",
            "base_currency",
            "base_amount",
            "quote_currency",
            "quote_amount",
        )
        read_only_fields = fields


class OrderRetrieveSerializer(serializers.ModelSerializer):
    confirmations = OrderConfirmationRetrieveSerializer(many=True, read_only=True)

    class Meta:
        model = Order
        fields = (
            "id",
            "wallet_id",
            "base_currency",
            "base_amount",
            "quote_currency",
            "quote_amount",
            "confirmations",
            "is_completed",
            "is_canceled",
            "is_done",
        )
        read_only_fields = fields
